// console.log("Send stars!");


let number = 8;
// let getCube = Math.pow(number,3);

console.log(`The cube of ${number} is ${Math.pow(number,3)}.`);

// [ houseNumber, street, barangay, municipality, Province, zipCode ]
let address = [ 2023, "Edison St.", "San Isidro", "Makati City", "Metro Manila", "1234" ];


let [ houseNumber, street, barangay, municipality, province, zipCode ] = address;

console.log(`I live at ${houseNumber} ${street} ${barangay}, ${municipality} ${province} ${zipCode}.`);


	// { name, species, weight, measurement }
let animal = {
	name: "Yuuki",
	species: "Dog",
	weight: "7.2 kg",
	measurement: "44 cm"
}

let {name, species, weight, measurement} = animal;

console.log(`My pet's name is ${name}. She's a ${species}. She weigh ${weight} and ${measurement} in lenght.`);


let arrayNumber = [1,2,3,4,5];

arrayNumber.forEach((num) => console.log(num));


let reduceNumber = arrayNumber.reduce((reduceNumber,num) => reduceNumber + num);
console.log(reduceNumber);


class Dog {
	constructor(name,age,breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let Yuuki = new Dog("Yuuki",7,"Shih Tzu");
console.log(Yuuki);